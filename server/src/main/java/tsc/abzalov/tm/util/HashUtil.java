package tsc.abzalov.tm.util;

import lombok.SneakyThrows;
import lombok.experimental.UtilityClass;
import lombok.val;
import lombok.var;
import org.jetbrains.annotations.NotNull;

import java.security.NoSuchAlgorithmException;

import static java.security.MessageDigest.getInstance;

@UtilityClass
public class HashUtil {

    @NotNull
    private static final String ALGORITHM = "MD5";

    @NotNull
    public static String hash(
            @NotNull String someStringToHash,
            @NotNull final Integer counter,
            @NotNull final String salt
    ) {
        for (var i = 0; i < counter; i++) someStringToHash = md5(salt + someStringToHash + salt);
        return someStringToHash;
    }

    @NotNull
    @SneakyThrows
    private static String md5(@NotNull final String hash) {
        try {
            @NotNull val md = getInstance(ALGORITHM);
            final byte[] bytes = md.digest(hash.getBytes());
            @NotNull val builder = new StringBuilder();

            for (val b : bytes) builder.append(Integer.toHexString((b & 0xFF) | 0x100), 1, 3);
            return builder.toString();
        } catch (NoSuchAlgorithmException exception) {
            throw new Exception(exception);
        }
    }

}
