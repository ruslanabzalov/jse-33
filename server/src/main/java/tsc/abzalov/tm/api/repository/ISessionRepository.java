package tsc.abzalov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.Session;

public interface ISessionRepository {

    void addSession(@NotNull Session session);

    void removeSession(@NotNull Session session);

    @Nullable
    Session findSession(@NotNull Session session);

}
