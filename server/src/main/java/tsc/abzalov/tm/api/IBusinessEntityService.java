package tsc.abzalov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.AbstractBusinessEntity;

import java.util.List;


public interface IBusinessEntityService<T extends AbstractBusinessEntity> extends IService<T> {

    long size(@Nullable String userId);

    boolean isEmpty(@Nullable String userId);

    int indexOf(@Nullable String userId, @Nullable T entity);

    @NotNull
    List<T> findAll(@Nullable String userId);

    @Nullable
    T findById(@Nullable String userId, @Nullable String id);

    @Nullable
    T findByIndex(@Nullable String userId, int index);

    @Nullable
    T findByName(@Nullable String userId, @Nullable String name);

    @Nullable
    T editById(@Nullable String userId, @Nullable String id,
               @Nullable String name, @Nullable String description);

    @Nullable
    T editByIndex(@Nullable String userId, int index,
                  @Nullable String name, @Nullable String description);

    @Nullable
    T editByName(@Nullable String userId, @Nullable String name, @Nullable String description);

    void clear(@Nullable String userId);

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByIndex(@Nullable String userId, int index);

    void removeByName(@Nullable String userId, @Nullable String name);

    @Nullable
    T startById(@Nullable String userId, @Nullable String id);

    @Nullable
    T endById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<T> sortByName(@Nullable String userId);

    @NotNull
    List<T> sortByStartDate(@Nullable String userId);

    @NotNull
    List<T> sortByEndDate(@Nullable String userId);

    @NotNull
    List<T> sortByStatus(@Nullable String userId);

}
