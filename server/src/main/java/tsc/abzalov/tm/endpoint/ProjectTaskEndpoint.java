package tsc.abzalov.tm.endpoint;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.endpoint.IEndpointLocator;
import tsc.abzalov.tm.api.endpoint.IProjectTaskEndpoint;
import tsc.abzalov.tm.exception.auth.AccessDeniedException;
import tsc.abzalov.tm.model.Project;
import tsc.abzalov.tm.model.Session;
import tsc.abzalov.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
public final class ProjectTaskEndpoint extends AbstractEndpoint implements IProjectTaskEndpoint {

    public ProjectTaskEndpoint() {
    }

    public ProjectTaskEndpoint(@NotNull final IEndpointLocator endpointLocator) {
        super(endpointLocator);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public int indexOfProjectTask(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "task") @Nullable final Task task) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectTaskService = getEndpointLocator().getProjectTaskService();
        return projectTaskService.indexOf(session.getUserId(), task);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public boolean hasData(@WebParam(name = "session") @Nullable final Session session) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectTaskService = getEndpointLocator().getProjectTaskService();
        return projectTaskService.hasData(session.getUserId());
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void addTaskToProjectById(@WebParam(name = "session") @Nullable final Session session,
                                     @WebParam(name = "projectId") @Nullable final String projectId,
                                     @WebParam(name = "taskId") @Nullable final String taskId) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectTaskService = getEndpointLocator().getProjectTaskService();
        projectTaskService.addTaskToProjectById(session.getUserId(), projectId, taskId);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public Project findTaskProjectById(@WebParam(name = "session") @Nullable final Session session,
                                                 @WebParam(name = "id") @Nullable final String id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectTaskService = getEndpointLocator().getProjectTaskService();
        return projectTaskService.findProjectById(session.getUserId(), id);
    }

    @Override
    @Nullable
    @WebMethod
    @SneakyThrows
    public Task findProjectTaskById(@WebParam(name = "session") @Nullable final Session session,
                                              @WebParam(name = "id") @Nullable final String id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectTaskService = getEndpointLocator().getProjectTaskService();
        return projectTaskService.findTaskById(session.getUserId(), id);
    }

    @NotNull
    @Override
    @WebMethod
    @SneakyThrows
    public List<Task> findProjectTasksById(@WebParam(name = "session") @Nullable final Session session,
                                                    @WebParam(name = "projectId") @Nullable final String projectId) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectTaskService = getEndpointLocator().getProjectTaskService();
        return projectTaskService.findProjectTasksById(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void deleteProjectById(@WebParam(name = "session") @Nullable final Session session,
                                  @WebParam(name = "id") @Nullable final String id) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectTaskService = getEndpointLocator().getProjectTaskService();
        projectTaskService.deleteProjectById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void deleteProjectTasksById(@WebParam(name = "session") @Nullable final Session session,
                                       @WebParam(name = "projectId") @Nullable final String projectId) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectTaskService = getEndpointLocator().getProjectTaskService();
        projectTaskService.deleteProjectTasksById(session.getUserId(), projectId);
    }

    @Override
    @WebMethod
    @SneakyThrows
    public void deleteProjectTaskById(@WebParam(name = "session") @Nullable final Session session,
                                      @WebParam(name = "projectId") @Nullable final String projectId) {
        if (getEndpointLocator() == null) throw new AccessDeniedException();
        getEndpointLocator().getSessionService().validate(session);
        @NotNull val projectTaskService = getEndpointLocator().getProjectTaskService();
        projectTaskService.deleteProjectTaskById(session.getUserId(), projectId);
    }

}
