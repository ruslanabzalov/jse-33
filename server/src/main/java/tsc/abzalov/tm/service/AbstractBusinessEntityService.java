package tsc.abzalov.tm.service;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.IBusinessEntityRepository;
import tsc.abzalov.tm.api.IBusinessEntityService;
import tsc.abzalov.tm.exception.data.*;
import tsc.abzalov.tm.model.AbstractBusinessEntity;

import java.util.List;
import java.util.Optional;

import static tsc.abzalov.tm.util.InputUtil.isIndexIncorrect;
import static tsc.abzalov.tm.util.LiteralConst.DEFAULT_DESCRIPTION;

public abstract class AbstractBusinessEntityService<T extends AbstractBusinessEntity> extends AbstractService<T>
        implements IBusinessEntityService<T> {

    @NotNull
    private final IBusinessEntityRepository<T> repository;

    public AbstractBusinessEntityService(@NotNull final IBusinessEntityRepository<T> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    @SneakyThrows
    public long size(@Nullable String userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        return repository.size(userId);
    }

    @Override
    @SneakyThrows
    public boolean isEmpty(@Nullable String userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        return repository.isEmpty(userId);
    }

    @Override
    @SneakyThrows
    public int indexOf(@Nullable String userId, @Nullable T entity) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        entity = Optional.ofNullable(entity).orElseThrow(EmptyEntityException::new);
        return repository.indexOf(userId, entity);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> findAll(@Nullable String userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        return repository.findAll(userId);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T findById(@Nullable String userId, @Nullable String id) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @Nullable val searchedEntity = repository.findById(userId, id);
        return Optional.ofNullable(searchedEntity).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T findByIndex(@Nullable String userId, int index) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);

        index = index - 1;
        if (isIndexIncorrect(index, size(userId))) throw new IncorrectIndexException(index);

        @Nullable val searchedEntity = repository.findByIndex(userId, index);
        return Optional.ofNullable(searchedEntity).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T findByName(@Nullable String userId, @Nullable String name) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        name = Optional.ofNullable(name).orElseThrow(EmptyNameException::new);

        @Nullable val searchedEntity = repository.findByName(userId, name);
        return Optional.ofNullable(searchedEntity).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T editById(@Nullable String userId, @Nullable String id,
                      @Nullable String name, @Nullable String description) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        name = Optional.ofNullable(name).orElseThrow(EmptyNameException::new);

        description = Optional.ofNullable(description).orElse(DEFAULT_DESCRIPTION);

        @Nullable val editedEntity = repository.editById(userId, id, name, description);
        return Optional.ofNullable(editedEntity).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T editByIndex(@Nullable String userId, int index, @Nullable String name, @Nullable String description) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);

        --index;
        if (isIndexIncorrect(index, size(userId))) throw new IncorrectIndexException(index);

        name = Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        description = Optional.ofNullable(description).orElse(DEFAULT_DESCRIPTION);

        @Nullable val editedEntity = repository.editByIndex(userId, index, name, description);
        return Optional.ofNullable(editedEntity).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T editByName(@Nullable String userId, @Nullable String name, @Nullable String description) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        name = Optional.ofNullable(name).orElseThrow(EmptyNameException::new);

        description = Optional.ofNullable(description).orElse(DEFAULT_DESCRIPTION);

        @Nullable val editedEntity = repository.editByName(userId, name, description);
        return Optional.ofNullable(editedEntity).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @SneakyThrows
    public void clear(@Nullable String userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        repository.clear(userId);
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable String userId, @Nullable String id) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        repository.removeById(userId, id);
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@Nullable String userId, int index) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);

        --index;
        if (isIndexIncorrect(index, size(userId))) throw new IncorrectIndexException(index);

        repository.removeByIndex(userId, index);
    }

    @Override
    @SneakyThrows
    public void removeByName(@Nullable String userId, @Nullable String name) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        name = Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        repository.removeByName(userId, name);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T startById(@Nullable String userId, @Nullable String id) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @Nullable val startedEntity = repository.startById(userId, id);
        return Optional.ofNullable(startedEntity).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Nullable
    @SneakyThrows
    public T endById(@Nullable String userId, @Nullable String id) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        id = Optional.ofNullable(id).orElseThrow(EmptyIdException::new);

        @Nullable val endedEntity = repository.endById(userId, id);
        return Optional.ofNullable(endedEntity).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByName(@Nullable String userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        @NotNull val entities = repository.findAll(userId);

        entities.sort((firstEntity, secondEntity) -> {
            @Nullable val firstEntityName = firstEntity.getName();
            @Nullable val secondEntityName = secondEntity.getName();
            if (firstEntityName == null || secondEntityName == null) return 0;
            return firstEntityName.compareTo(secondEntityName);
        });

        return entities;
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByStartDate(@Nullable String userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        @NotNull val entities = repository.findAll(userId);

        entities.sort((firstEntity, secondEntity) -> {
            @Nullable val firstEntityStartDate = firstEntity.getStartDate();
            @Nullable val secondEntityStartDate = secondEntity.getStartDate();
            if (firstEntityStartDate == null || secondEntityStartDate == null) return 0;
            return firstEntityStartDate.compareTo(secondEntityStartDate);
        });

        return entities;
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByEndDate(@Nullable String userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        @NotNull val entities = repository.findAll(userId);

        entities.sort((firstEntity, secondEntity) -> {
            @Nullable val firstEntityEndDate = firstEntity.getEndDate();
            @Nullable val secondEntityEndDate = secondEntity.getEndDate();
            if (firstEntityEndDate == null || secondEntityEndDate == null) return 0;
            return firstEntityEndDate.compareTo(secondEntityEndDate);
        });

        return entities;
    }

    @Override
    @NotNull
    @SneakyThrows
    public List<T> sortByStatus(@Nullable String userId) {
        userId = Optional.ofNullable(userId).orElseThrow(EmptyIdException::new);
        @NotNull val entities = repository.findAll(userId);

        entities.sort((firstEntity, secondEntity) -> {
            @NotNull val firstEntityStatus = firstEntity.getStatus();
            @NotNull val secondEntityStatus = secondEntity.getStatus();
            return firstEntityStatus.compareTo(secondEntityStatus);
        });

        return entities;
    }

}
