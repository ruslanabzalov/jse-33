package tsc.abzalov.tm.command.user;

import lombok.SneakyThrows;
import lombok.val;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.service.IServiceLocator;
import tsc.abzalov.tm.command.AbstractCommand;
import tsc.abzalov.tm.enumeration.CommandType;
import tsc.abzalov.tm.exception.auth.CannotDeleteCurrentUserException;

import static tsc.abzalov.tm.enumeration.CommandType.ADMIN_COMMAND;
import static tsc.abzalov.tm.util.InputUtil.inputLogin;


public final class UserDeleteByLoginCommand extends AbstractCommand {

    public UserDeleteByLoginCommand(@NotNull final IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @Override
    @NotNull
    public String getCommandName() {
        return "delete-user-by-login";
    }

    @Override
    @Nullable
    public String getCommandArgument() {
        return null;
    }

    @Override
    @NotNull
    public String getDescription() {
        return "Delete user by login.";
    }

    @Override
    @NotNull
    public CommandType getCommandType() {
        return ADMIN_COMMAND;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("DELETE USER BY LOGIN");
        @NotNull val userLogin = inputLogin();
        @NotNull val session = getServiceLocator().getSession();
        @NotNull val userId = session.getUserId();
        @NotNull val userEndpoint = getServiceLocator().getUserEndpoint();

        @NotNull val currentUserLogin = userEndpoint.findUserById(session, userId).getLogin();
        if (userLogin.equals(currentUserLogin)) throw new CannotDeleteCurrentUserException();

        @NotNull val adminEndpoint = getServiceLocator().getAdminEndpoint();
        adminEndpoint.deleteUserByLogin(session, userLogin);

        System.out.println("User Successfully Deleted By Admin.\n");
    }

}
